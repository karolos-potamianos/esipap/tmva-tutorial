# TMVA Tutorial

This is a TMVA tutorial given for the course ESIPAP 2020.

The tutorials consist in Jupyter notebooks, which can be viewed online, 
and exectued locally or on dedicated infrastructure such as SWAN 
(https://swan.cern.ch).

A User's Guide for TMVA is avaiable at (https://root.cern/download/doc/tmva/TMVAUsersGuide.pdf).

## Prerequisites

This tutorial relies on a recent versions of ROOT (>= 6) and on Python 2.7
(or Python 3 if your version of ROOT was built against it).
In addition, some of the tools require the installation of Keras and Tensorflow.
The latter can be installe dusing

```
pip install --user jupyter
pip install --user keras 
pip install --user tensorflow==1.15
```

To install `pip`, see (https://pip.pypa.io/en/stable/installing/).

## Running the tutorial

Once this repository is checked out, e.g. using the following command in the
terminal:

```
git clone https://gitlab.com/esipap/tmva-tutorial.git
```

you can start the Jupyter interpreter using

```
cd tmva-tutorial # To position yourself in the folder with the notebooks
root --notebook
```

Alternatively, you can use

```
cd tmva-tutorial # To position yourself in the folder with the notebooks
jupyter notebook
```

A browser window will open to a page where you can run the various notebooks.

## Examples in this tutorial

### TMVA Classification : Toys

The tutorial [TMVA-Classification-Keras.ipynb](https://gitlab.com/esipap/tmva-tutorial/blob/master/TMVA-Classification-Keras.ipynb) applies some of the concepts behind TMVA to toy distributions.

### TMVA Classification : Higgs

This set is an example of classification of (simulated) collision events 
(10000 Higgs signal events and 10000 background events).

The first tutorial covers the basiscs as well as a simple BDT. 
The second compares several parameters to the BDT.
The third compares various algorhitms (BDT and NN).

* [TMVA-Classification-Higgs-BDT.ipynb](https://gitlab.com/esipap/tmva-tutorial/blob/master/TMVA-Classification-Higgs-BDT.ipynb)
* [TMVA-Classification-Higgs-BDT-Comparison.ipynb](https://gitlab.com/esipap/tmva-tutorial/blob/master/TMVA-Classification-Higgs-BDT-Comparison.ipynb)
* [TMVA-Classification-Higgs-MVA-Comparison.ipynb](https://gitlab.com/esipap/tmva-tutorial/blob/master/TMVA-Classification-Higgs-MVA-Comparison.ipynb)
 
## Additional information

Additional tutorials of TMVA are available in the `tutorials/tmva` directory of 
your ROOT installation (`$ROOTSYS/tutorials/tmva`) as well as in the main ROOT 
git repository. You can find them also [here](https://root.cern/doc/master/group__tutorial__tmva.html).

These can be launched from a terminal using ROOT, for example

```
root -l $ROOTSYS/tutorials/tmva/TMVACrossValidation.C
```