# ESIPAP TMVA Lab

Contact: Karolos Potamianos [<karolos.potamianos@cern.ch>](mailto:karolos.potamianos@cern.ch)

## Setup

This section contains multiple options to run the tutorials, from running Docker (recommended), or using VirtualBox, or using CERN SWAN (only with a CERN account).

### CERN SWAN:

**Note: to use SWAN, you need a CERN computing account**

- Go to https://swan.cern.ch
- Select an instance configuration (the default is enough)
- If not already done, download this repository into SWAN, by clicking on the cloud icon (top right, next to the + sign) and entering this repository's URL
- Now you have a folder named `tmva-tutorial` that is also synced to your CERNbox space at `SWAN_projects/tmva-tutorial` (or, from `lxplus`, `/eos/user/${USER:0:1}/${USER}/SWAN_projects/tmva-tutorial`).

### VirtualBox:

- Download the image: https://cernbox.cern.ch/index.php/s/xYV09UpR4JZlg5G
- Extract it using `7z`
- Download [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and install it
- Create a VM using the extracted volume (`hdi` file) as disk (Settings > Storage > Controller: SATA and click on the + sign next to a spinning disk)
- Run the VM; the password to log in is `osboxes.org` (and the login is `osboxes.org`).

### Docker:

You can run this on your local machine, once Docker is installed. To install Docker, follow the instructions [here](https://docs.docker.com/get-docker/).

Pulling the code from the repository:

```
git clone https://gitlab.cern.ch/karolos-potamianos/esipap/tmva-tutorial.git
cd tmva-tutorial # If you choose not to enter the directory, you'll need to adjust the parameters of docker -v
```

Running the container:

```
docker run --rm -it -v "${PWD}":/tmva-tutorial rootproject/root:latest /bin/bash
```

To get X11 forwarding for graphics, follow the instructions available [here](https://hub.docker.com/r/rootproject/root). You might need to enable network connectivity to your local X11 server (e.g. XQuartz on macOS).

Once you've set up the proper X11 settings, you can run, e.g. on macOS:

```
docker run --rm -it -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$ip:0 -v "${PWD}":/tmva-tutorial rootproject/root:latest /bin/bash
```

### Running as a non-root user

In the above examples, the container is run as root; this isn't a problem because the container is a restricted environment, but in case you want to run as a user, you can use the following. Note that the Docker image used is different because the Ubuntu version doesn't map all users nicely.

```
docker run --rm -it --user $(id -u) -v "${PWD}":/tmva-tutorial rootproject/root:6.24.06-centos7 /bin/bash
```

or, with networking on macOS

```
docker run --rm -it --user $(id -u) -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$ip:0 -v "${PWD}":/tmva-tutorial rootproject/root:6.24.06-centos7 /bin/bash
```

## Running the tutorials

### Executing the examples

#### SWAN:

In SWAN, you can simply click on the Python Notebooks (`.ipynb` files) which will open Python Notebook session with the given file. You can then execute the script.

### VirtualBox:

Open the terminal and run the following commands, then you can run the exercises:

```
cd ~/esipap
source setup.sh
cd tmva-tutorial
python3 TMVA-Classification-Higgs-BDT.py
# or root --notebook
```

You must run these every time you open a new terminal to pick up the right environment.

#### Docker:

Once you are in the container and in the `/tmva-tutorial` folder, you can run

```
python3 TMVA-Classification-Higgs-BDT.py
```
which will run the same code as in the Notebook and produce output files, which you can browse.


### Interpreting the results

Once you've executed the scripts, either in the Notebook or in the command line, have a look at the output to understand what it's doing.

Whether you run in SWAN or Docker, you'll have after running `MVA-Classification-Higgs-BDT` a new file called `TMVA-Classification-Higgs-BDT.root` which you can open (you'll need a local setup for that in any case) using:

```
root -l -e 'TMVA::TMVAGui("TMVA-Classification-Higgs-BDT.root")'
```

### Using TMVA to compare MVAs

When running locally, you might encounter issues, when running

```
python3 TMVA-Classification-Higgs-MVA-Comparison.py 
```

or

```
python3 TMVA-Classification-Keras.py
```

This is because the packages `sklearn` and `keras` are not installed in the container.

You should then run (unfortunately, this needs to be repeated in each session and is not persistent accross container restarts):

```
python3 < <(curl -s https://bootstrap.pypa.io/get-pip.py)
pip install sklearn keras tensorflow
```

Note: when using the `centos` container, you can use pip directly, and you can even setup a virtualenv using:

```
python3 -m venv venv
source ./venv/bin/activate
pip install sklearn keras tensorflow
```

then you only need to run `source ./venv/bin/activate` in new container sessions.

For the `latest` (Ubuntu-based) container, you'll need to run `apt-get update && apt install -y python3-venv` before you can use the virtualenv. Unfortunately, this must be done each time you restart the container. If this is a problem, you can use a `Dockerfile` (see instructions) to create your own custom image.
