#!/usr/bin/env python
# coding: utf-8

# # Classification of events using TMVA : Boosted Decision Trees
# 
# In this example, we use the [Toolkit for Multivariate Data Analysis with ROOT (TMVA)](http://root.cern/tmva) to classify events from particle collisions to determine whether they contain Higgs bosons.
# 
# In this tutorial we'll learn how to tell TMVA to process data and we'll train a Boosted Decision Tree (BDT).
# 
# There are many resources available online regarding TMVA, including it's [User's Guide](https://root.cern/download/doc/tmva/TMVAUsersGuide.pdf).
# 

# In[1]:


# Importing the ROOT module
import ROOT
# Importing the TMVA, TFile (to open ROOT files) and TCut (to filter data)
from ROOT import TMVA, TFile, TCut

# Initialising TMVA
TMVA.Tools.Instance()
TMVA.PyMethodBase.PyInitialize()


# ## Creating the TMVA Factory
# 
# We now need to create a *Factory*, which will handle our interaction with TMVA and save the various inputs and outputs to the specified file (here `TMVA-Classification-Higgs-BDT.root`). 
# 
# We'll set up the factory to perform a *classification analysis* to discriminate the *signal* events against the *background* events.
# 
# We've configured the factory to test several transformations:
# - I: identity
# - D: decorrelation
# - P: PCA
# - U: uniform
# - G: Gaussian
# 
# 
# Note: there is a more Pythonic version, but there were some issues running this with the latest ROOT version.
# 
# ```
# %jsmva
# outputFile = "TMVA-Classification-Higgs.root"
# factory = TMVA.Factory("TMVAClassification", TargetFile=outputFile,
#                        V=False, Color=True, DrawProgressBar=True, 
#                        Transformations=["I", "D", "P", "G", "D"],
#                        AnalysisType="Classification")
# ```

# In[ ]:


# This will overwrite the file at each execution
outputFile = TFile( "TMVA-Classification-Higgs-BDT.root", "RECREATE" ) 
factory = TMVA.Factory( "TMVAClassification", outputFile,
                        "!V:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification" )


# ## Reading in the data
# 
# We need to import our data to run our algorithms on. This is done using a *DataLoader* to import our data. In this example, we'll use data from a ROOT file (`Higgs_data.root`). This file contains two trees: one with the signal events and one with background events.

# In[ ]:


inputFileName = "Higgs_data.root"
inputFile = TFile.Open( inputFileName )

# Retrieve input trees
sigTree = inputFile.Get("sig_tree")
bkgTree = inputFile.Get("bkg_tree")

#Instantiating the DataLoader
loader = TMVA.DataLoader("dataset")


# Let us first have a look at the data we are using, by printing some information on the TTree.

# In[ ]:


# Dumping some information about the TTree
sigTree.Print()


# Looking at the dump, we noticae that there 10000 events (number of entries) for which we have 28 variables available, corresponding to physical quantities related to the events.
# 
# Let us have a closer look at the variables.

# In[ ]:


# Check the ROOT manual for more info on TCanvas and TTree::Draw
# e.g. https://root.cern.ch/doc/master/classTTree.html
cInput = ROOT.TCanvas("Input Variables", "", 1200, 2100)
cInput.Divide(4,7) # Dividing the canvas into 28
ROOT.gStyle.SetOptStat(0) # Removing display of statistics for each histogram

# Ensuring we use a different color for drawing the contents of the signal and background trees
sigTree.SetLineColor(ROOT.kRed)
bkgTree.SetLineColor(ROOT.kBlue)

# Looping over all the variables available in the TTree
for i,v in enumerate(sigTree.GetListOfBranches()):
    cInput.cd(i+1) # Selecting the sub-canvas (TPad) to draw into
    varName = v.GetName()
    sigTree.Draw(varName)
    bkgTree.Draw(varName, "", "same")
cInput.Draw()
cInput.Print(".pdf") # This saves a PDF file of the canvas


# It is very important to look at the data beforehand and make sure it is well understood/modelled. In this simplified example already, we see that some variables do not provide a strong discriminating power.
# 
# Looking at the above, we see that some variables have some discrimiation potential: m_jj, m_jjj, m_lv, m_jlv, m_bb, m_wbb, m_wwbb. We will be using those in this tutorial.
# 
# Before adding the data to our loader, we might want to weight the events. This is needed for example when we simulate (using Monte Carlo event generators) many more events than what we expect to see in the data, which we need to scale in order to match the expectation in the data. Moreover, it is often needed to use many independent events to train machine learning algorithms.
# 
# We pick a few variables (present in the TTree) to use when running our machine learning.
# 
# We also have the possibility to select events based on some criterion (e.g. "m_jj>100") which we can speficy using a TCut object.

# In[ ]:


# Global event weights per tree: useful to adjust the event weight to match a given luminosity
sigWeight = 1.0
bkgWeight = 1.0

# Note: one can add any number of trees (e.g. multiple backgrounds)
loader.AddSignalTree    ( sigTree, sigWeight )
loader.AddBackgroundTree( bkgTree, bkgWeight )

# Adding variables (here corresponding to the branches of the TTree)
loader.AddVariable("m_jj")
loader.AddVariable("m_jjj")
loader.AddVariable("m_lv")
loader.AddVariable("m_jlv")
loader.AddVariable("m_bb")
loader.AddVariable("m_wbb")
loader.AddVariable("m_wwbb")


# Apply additional cuts on the signal and background samples (can be different)
sigCut = TCut("")   ## for example: TCut sigCut = "m_jj>100"
bkgCut = TCut("")   ## for example: TCut bkgCut = "m_jj>100"


# We will now prepare the *training* and *testing* trees to run the machine learning algorithms on.
# We will use 7000 events from each signal and background trees for the traning sample; the remainder will be used fr the testing sample (as we didn't specify explicitly how many we wanted). The events will be chosen at random.

# In[ ]:


loader.PrepareTrainingAndTestTree( sigCut, bkgCut,
         "nTrain_Signal=7000:nTrain_Background=7000:SplitMode=Random:"
         "NormMode=NumEvents:!V" )


# ## Using machine learning algorithms with TMVA
# 
# We can now use various algorithms to classify events according to their signal or background characteristic. In order to use an algorithm within TMVA it has to be booked using the TMVA Factory's `BookMethod`.
# 
# We start with a Boosted Decison Tree (BDT). Details regarding the options can be found in the [TMVA User's Guide](https://root.cern.ch/download/doc/tmva/TMVAUsersGuide.pdf) under the BDT section.

# In[ ]:


factory.BookMethod(loader,ROOT.TMVA.Types.kBDT, "BDT",
          "!V:NTrees=200:MinNodeSize=2.5%:MaxDepth=2:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:"
          "BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20" )


# The BookMethod function already provides some information regarding the dataset, such as the correlation matrices for the signal and background. These are important to look at to understand how the variables are correlated with each other.
# 
# We can now run the algorithms.

# In[ ]:


factory.TrainAllMethods()


# In[ ]:


factory.TestAllMethods()


# In[ ]:


factory.EvaluateAllMethods()


# The traninig is completed. The output it stored in the file we specified when creating the factory: `TMVA-Classification-Higgs.root`.
# 
# We can take a closer look at the performance of our algorithm by plotting the ROC (received operating characteristic) curve which shows how much background rejection our algorithm achieves for a given signal efficiency.
# 
# This metric is usually OK to compare algorithms but ultimately considerations such as the uncertainties on the background are necessary to assess whether it's more important to have a high signal efficiency or a high background rejection.

# In[ ]:


cROC = factory.GetROCCurve(loader)
cROC.Draw()
cROC.Print(".pdf") # This saves a PDF file of the canvas


# In[ ]:


# Taking a peek at the output file
ROOT.gDirectory.ls()


# In[ ]:


corrMatS = ROOT.gDirectory.Get("CorrelationMatrixS")
corrMatB = ROOT.gDirectory.Get("CorrelationMatrixB")

ROOT.gStyle.SetPalette(ROOT.kRainBow)
corrMat = ROOT.TCanvas("Correlation Matrices", "", 800, 400)
corrMat.Divide(2,1)
corrMat.cd(1)
corrMatS.Draw("colz text")
corrMat.cd(2)
corrMatB.Draw("colz text")
corrMat.Draw()
corrMat.Print(".pdf")


# In[ ]:


# Closing the file where we have the methods trained
outputFile.Close()
#factory.Delete()


# This tutorial introduced some of the basic concepts behind TMVA and BDTs. In the next tutorial, we will compare various algorithms with each other.

# ### Follow-up
# 
# One can adjust the various parameters of the BDT and see how things compare with each other. 
# 
# For example, one can adjust the number events to use in the training and testing sample, or the number of trees in the BDT to see the impact. Try to play with other parameters to get a feeling of how the algorithm works.
# 
# ### Exploring the methods using the TMVAGui
# 
# TMVA comes with the TMVAGui, which helps understand how the algorithms behave. To run it, simply run
# ```
# root -l -e 'TMVA::TMVAGui("TMVA-Classification-Higgs-BDT.root")'
# ```
# from your terminal.
