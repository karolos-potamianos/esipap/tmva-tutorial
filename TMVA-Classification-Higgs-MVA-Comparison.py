#!/usr/bin/env python
# coding: utf-8

# # Comparing multiple MVA algorithms with TMVA
# 
# In this tutorial we will compare several algorithms with the [Toolkit for Multivariate Data Analysis with ROOT (TMVA)](http://root.cern/tmva) to classify events from particle collisions to determine whether they contain Higgs bosons.
# 
# We will skip the analysis of the input dataset (covered in another tutorial) and proceed to the comparison of several algorithms, such as Boosted Decision Trees (BDT), Neural Networks (NN) and Deep Neural Networks (DNN).

# In[1]:


# Importing the ROOT module
import ROOT
# Importing the TMVA, TFile (to open ROOT files) and TCut (to filter data)
from ROOT import TMVA, TFile, TCut

# Initialising TMVA
TMVA.Tools.Instance()
TMVA.PyMethodBase.PyInitialize()


# In[2]:


# This will overwrite the file at each execution
outputFile = TFile( "TMVA-Classification-Higgs-MVA-Comparison.root", "RECREATE" ) 
factory = TMVA.Factory( "TMVAClassHiggsMVAComp", outputFile,
                        "!V:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification" )

# Opening the input data
inputFileName = "Higgs_data.root"
inputFile = TFile.Open( inputFileName )

# Retrieve input trees
sigTree = inputFile.Get("sig_tree")
bkgTree = inputFile.Get("bkg_tree")

#Instantiating the DataLoader
loader = TMVA.DataLoader("dataset")

# Global event weights per tree: useful to adjust the event weight to match a given luminosity
sigWeight = 1.0
bkgWeight = 1.0

# Note: one can add any number of trees (e.g. multiple backgrounds)
loader.AddSignalTree    ( sigTree, sigWeight )
loader.AddBackgroundTree( bkgTree, bkgWeight )

# Adding variables (here corresponding to the branches of the TTree)
loader.AddVariable("m_jj")
loader.AddVariable("m_jjj")
loader.AddVariable("m_lv")
loader.AddVariable("m_jlv")
loader.AddVariable("m_bb")
loader.AddVariable("m_wbb")
loader.AddVariable("m_wwbb")

# Apply additional cuts on the signal and background samples (can be different)
sigCut = TCut("")   ## for example: TCut sigCut = "m_jj>100"
bkgCut = TCut("")   ## for example: TCut bkgCut = "m_jj>100"


loader.PrepareTrainingAndTestTree( sigCut, bkgCut,
         "nTrain_Signal=7000:nTrain_Background=7000:SplitMode=Random:"
         "NormMode=NumEvents:!V" )


# Booking a BDT
factory.BookMethod(loader,ROOT.TMVA.Types.kBDT, "BDT",
          "!V:NTrees=200:MinNodeSize=2.5%:MaxDepth=2:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:"
          "BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20" )


# 

# In[3]:


# Booking PyGTB (Gradient Trees Boosted), see http://oproject.org/pages/PyMVA.html
factory.BookMethod(loader, ROOT.TMVA.Types.kPyGTB, "PyGTB",
          "H:!V:VarTransform=G:NEstimators=400:LearningRate=0.1:MaxDepth=3" )

# Booking PyRandomForest, see http://oproject.org/pages/PyMVA.html
factory.BookMethod(loader, ROOT.TMVA.Types.kPyRandomForest, "PyRandomForest",
          "!V:VarTransform=G:NEstimators=400:Criterion=gini:MaxFeatures=auto:MaxDepth=6:MinSamplesLeaf=3:"
          "MinWeightFractionLeaf=0:Bootstrap=kTRUE" )

# Booking PyAdaBoost, see http://oproject.org/pages/PyMVA.html      
factory.BookMethod(loader, ROOT.TMVA.Types.kPyAdaBoost, "PyAdaBoost","!V:VarTransform=G:NEstimators=400" )


# In[4]:


# Booking several instances Neural Networks using different interfaces

# Built-in Multi-Layer Perceptron (MLP)
factory.BookMethod(loader, ROOT.TMVA.Types.kMLP, "MLP",
          "!H:!V:NeuronType=tanh:VarTransform=N:NCycles=100:HiddenLayers=N+5:TestRate=5:!UseRegulator" );


# In[5]:


# Booking a Deep Neural Network (DNN)

# Defining training strategy using ADAM optimiser without regularisation
trainAdamNoReg  = "Optimizer=ADAM,LearningRate=1e-3,Momentum=0.,Regularization=None,WeightDecay=1e-4,"
trainAdamNoReg += "DropConfig=0.+0.+0.+0.,MaxEpochs=30,ConvergenceSteps=10,BatchSize=32,TestRepetitions=1"
 
# Defininig training strategy using ADAM optimiser with L2 regularisation
trainAdamL2  = "Optimizer=ADAM,LearningRate=1e-3,Momentum=0.,Regularization=L2,WeightDecay=1e-4,"
trainAdamL2 += "DropConfig=0.0+0.0+0.0+0,MaxEpochs=20,ConvergenceSteps=10,BatchSize=1000,TestRepetitions=1"            

# Configuring DNN                                                                                                                                                             
dnnOptions = "!H:V:ErrorStrategy=CROSSENTROPY:VarTransform=G:WeightInitialization=XAVIER::Architecture=CPU"

# Defining input layer
dnnOptions +=  ":" + "InputLayout=1|1|7"
dnnOptions +=  ":" + "BatchLayout=1|32|7"
dnnOptions +=  ":" + "Layout=DENSE|64|TANH,DENSE|64|TANH,DENSE|64|TANH,DENSE|64|TANH,DENSE|1|LINEAR"
dnnOptions +=  ":" + "TrainingStrategy=" + trainAdamNoReg


# Booking the model with TMVA              
factory.BookMethod(loader, ROOT.TMVA.Types.kDL, "DL_CPU", dnnOptions)


# ## Using external libraries to train (e.g. Keras)
# 
# There are many algorithms and implementations out there to perform machine learning tasks. Some of the more famous frameworks are Keras, Tensorflow. Other libraries such as PyTorch target computer vision and natural language processing (NLP).
# 
# TMVA has the ability to book algorithms from Keras using the Tensorflow backend.
# 
# Please note that this won't work on your system if you don't have some of the packages installed. You can install them using the commands below:
# 
# ```
# pip install --user tensorflow==1.15
# pip install --user keras
# ```
# 
# You might need to install `pip`. Instructions are available [here](https://pip.pypa.io/en/stable/installing/).

# In[6]:


# Telling Keras to use the Tensorflow backend
import os
# For this purpose, we set the environment variable KERAS_BACKEND
os.environ["KERAS_BACKEND"] = "tensorflow"

# Importing Keras models, optimizers and layers
from keras.models import Sequential
from tensorflow.keras.optimizers import Adam, SGD
from keras.layers import Input, Dense, Dropout, Flatten, Conv2D, MaxPooling2D, Reshape

# Setting up our model
model = Sequential()
# We start with a model with 7 inputs (corresponding to our 7 input variables) and 64 outputs
# We will use tanh activation. For more info, see https://keras.io/activations/
# For more information about the parameters, check https://keras.io/layers/core/
model.add(Dense(64, kernel_initializer='glorot_normal', activation='tanh', input_dim=7))
# We add another layer. Note that the input dimension doesn't need to be specified anymore!
model.add(Dense(64, kernel_initializer='glorot_normal', activation='tanh'))
# And another layer
model.add(Dense(64, kernel_initializer='glorot_normal', activation='tanh'))
# Finally, our last layer will have 2 outputs (corresponding to signal and background)
# The softmax function will normalise the output into a probability distribution
model.add(Dense(2, kernel_initializer='glorot_uniform', activation='softmax'))

# We define the loss function and optimisation strategy
model.compile(loss='categorical_crossentropy', optimizer=Adam(), metrics=['categorical_accuracy',])

# Keras stores the model in the H5 format, so we need to specify a file name
model.save('higgs_dnn_1.h5')

# Print summary of model
model.summary()

factory.BookMethod(loader, ROOT.TMVA.Types.kPyKeras, "Keras_Dense",
          "H:!V:VarTransform=G:FilenameModel=higgs_dnn_1.h5:"
          "NumEpochs=30:BatchSize=32:TriesEarlyStopping=10")


# In[7]:


factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()


# Each method produced a weight file in XML format which can be re-used to apply the method to other inputs. In addition a C++ standalone code is created to integrate the methods into other programmes.

# In[8]:


# We can now compare with our first BDT
cROC = factory.GetROCCurve(loader)
cROC.Draw()


# In[9]:


# Closing the file where we have the methods trained
outputFile.Close()
#factory.Delete()


# ## Follow-up
# 
# There are many, many parameters to all these models.
# 
# ### Exploring the methods using the TMVAGui
# 
# TMVA comes with the TMVAGui, which helps understand how the algorithms behave. To run it, simply run
# ```
# root -l -e 'TMVA::TMVAGui("TMVA-Classification-Higgs-MVA-Comparison.root")'
# ```
# from your terminal.
# 
# ### Dropout
# 
# For example, Deep Neural Networks are prompt to overfitting because of the large amount of parameters, esp. when the input dataset is small. You can try to see the impact yourself by reducing the size of the training and testing samples.
# 
# 
# To reduce over-fitting, we can use ensembles of NN with different configurations. However, these require significan computing resources to train and maintain multiple models. Dropout is a technique which randomly disables some of the hidden nodes during training, which simulates having a large number of different network architectures. It offers an effective and computationally cheap regularisation method to reduce overfitting and improve generalisation error.
# 
# One can quickly add a dropout to the Keras example by simply adding
# ```
# model.add(Dropout(0.2))
# ```
# before the layer where the dropping of nodes should happend (i.e. the function disables inputs). In the example above, 20% of the inputs will be dropped. For more information, see https://keras.io/layers/core/
